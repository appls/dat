#add bash completion
if [ -f /usr/share/bash-completion/bash_completion ]; then
	source /usr/share/bash-completion/bash_completion
fi

#add kubectl
if out=$(kubectl) ; then
	source <(kubectl completion bash)
fi

# some abbreviations to be quicker
alias ll='ls -ll'
alias la='ls -la'
alias py3='python3'

#do some coloring
alias ls='ls --color=auto'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'